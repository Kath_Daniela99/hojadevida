package com.example.curriculum_app1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity2_DatosP extends AppCompatActivity {
    Button anterior;
    Button siguienteRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2__datos_p);


        anterior= (Button)findViewById(R.id.button_VolverP);
        anterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent anterior = new Intent(MainActivity2_DatosP.this, MainActivity.class);
                startActivity(anterior);

            }
        });

        siguienteRef= (Button)findViewById(R.id.button_sig_refe);
        siguienteRef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent siguienteRef = new Intent(MainActivity2_DatosP.this, MainActivity3_ref.class);
                startActivity(siguienteRef);

            }
        });

    }
}