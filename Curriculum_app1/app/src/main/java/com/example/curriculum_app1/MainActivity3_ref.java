package com.example.curriculum_app1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity3_ref extends AppCompatActivity {
    Button volverDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity3_ref);
        volverDatos= (Button)findViewById(R.id.button_volverDatos);
        volverDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent volverDatos = new Intent(MainActivity3_ref.this, MainActivity2_DatosP.class);
                startActivity(volverDatos);

            }
        });



    }
}